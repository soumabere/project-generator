<?php


try
{
    $database = new PDO('mysql:host=localhost:3306;dbname=test;charset=utf8', 'root', '');
    $database -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
}

$database->query('TRUNCATE TABLE projects');